//_______________________render work section gallery
const pic = [
    {category: 'graphic',
    image: "images/work__content_img12.jpg"},
    {category: 'web',
    image: "images/work__content_img2.jpg"},
    {category: 'landing',
    image: "images/work__content_img3.jpg"},
    {category: 'w_press',
    image: "images/work__content_img4.jpg"},
    {category: 'w_press',
    image: "images/work__content_img5.jpg"},
    {category: 'graphic',
    image: "images/work__content_img6.jpg"},
    {category: 'web',
    image: "images/work__content_img7.jpg"},
    {category: 'landing',
    image: "images/work__content_img8.jpg"},
    {category: 'w_press',
    image: "images/work__content_img9.jpg"},
    {category: 'graphic',
    image: "images/work__content_img10.jpg"},
    {category: 'web',
    image: "images/work__content_img11.jpg"},
    {category: 'landing',
    image: "images/work__content_img12.jpg"},
];


function defRender(){
    const content = document.querySelector(".work__tabs_content");
    pic.forEach(item => {
        content.innerHTML += `<div class="w__tabs-wrapper" data-category="${item.category}">
        <img src="${item.image}" alt="" class="work__tabs_img img">
    </div>`
    });
}
defRender();


function loadDefImage(category) {
    let res = [];
    pic.forEach((el)=>{
        if(el.category === category){
            res.push(el);
        }
    })

    return res;
}



//_______________________incoming data arr
const incomingData = [
    {category: 'graphic',
    image: "images/work__content_img12.jpg"},
    {category: 'web',
    image: "images/work__content_img2.jpg"},
    {category: 'landing',
    image: "images/work__content_img3.jpg"},
    {category: 'w_press',
    image: "images/work__content_img4.jpg"},
    {category: 'w_press',
    image: "images/work__content_img5.jpg"},
    {category: 'graphic',
    image: "images/work__content_img6.jpg"},
    {category: 'web',
    image: "images/work__content_img7.jpg"},
    {category: 'landing',
    image: "images/work__content_img8.jpg"},
    {category: 'w_press',
    image: "images/work__content_img9.jpg"},
    {category: 'graphic',
    image: "images/work__content_img10.jpg"},
    {category: 'web',
    image: "images/work__content_img11.jpg"},
    {category: 'landing',
    image: "images/work__content_img12.jpg"},
    {category: 'graphic',
    image: "images/work__content_img12.jpg"},
    {category: 'web',
    image: "images/work__content_img2.jpg"},
    {category: 'landing',
    image: "images/work__content_img3.jpg"},
    {category: 'w_press',
    image: "images/work__content_img4.jpg"},
    {category: 'w_press',
    image: "images/work__content_img5.jpg"},
    {category: 'graphic',
    image: "images/work__content_img6.jpg"},
    {category: 'web',
    image: "images/work__content_img7.jpg"},
    {category: 'landing',
    image: "images/work__content_img8.jpg"},
    {category: 'w_press',
    image: "images/work__content_img9.jpg"},
    {category: 'graphic',
    image: "images/work__content_img10.jpg"},
    {category: 'web',
    image: "images/work__content_img11.jpg"},
    {category: 'landing',
    image: "images/work__content_img12.jpg"},
];



//______________________________________________section services
const tabs = document.querySelector(".tabs");

const tabsClickHandler = (e) => {
    const active = document.querySelector(".tabs_hov.active");
    const activeContent = document.querySelector(".content__wrapper.active")
    const item = e.target;
    const tabsContent = document.querySelectorAll(".tabs__content .content__wrapper");
    
    active.classList.remove("active");
    activeContent.classList.remove("active");
    item.classList.add("active");
    tabsContent[item.dataset.listNumber].classList.add("active");
};
 
tabs.addEventListener("click", tabsClickHandler);
//______________________________________________section work
const workTabs = document.querySelector(".tabs__centered .tabs");

const workTabsClickHandler = (e) => {
    const content = document.querySelector(".work__tabs_content");
    const active = document.querySelector(".work_tabs.active");
    const item = e.target;
    
    active.classList.remove("active");
    item.classList.add("active");
    content.innerHTML = ""
    if(item.dataset.category === "all"){
        defRender();
    }else{
        loadDefImage(item.dataset.category).forEach(el => {
            content.innerHTML += `<div class="w__tabs-wrapper" data-category="${el.category}">
            <img src="${el.image}" alt="" class="work__tabs_img img"> </div>`
        });
    }
};
 
workTabs.addEventListener("click", workTabsClickHandler);


//______________________________________________section work btn


const workBtn = document.querySelector(".work__btn");
workBtn.addEventListener("click", workBtnClickHandler)

function workBtnClickHandler(){
    const active = document.querySelector(".work_tabs.active")
    const content = document.querySelector(".work__tabs_content");
    
    

    if(active.dataset.category === "all"){
        let contentLimit = incomingData.splice(0, 12);
        contentLimit.forEach(item => {
            pic.push(item);
        });
        content.innerHTML = "";
        defRender();
    }else{
        const checkLimit = [];
        for(let i = 0; i < incomingData.length; i++){
            if(incomingData[i].category === active.dataset.category){
                checkLimit.push(incomingData.splice(i, 1)[0]);
            }
            if(checkLimit.length === 3){
                break;
            }
        }
        checkLimit.forEach((el)=>{
            pic.push(el);
        })
        content.innerHTML = "";
        loadDefImage(active.dataset.category).forEach(el => {
            content.innerHTML += `<div class="w__tabs-wrapper" data-category="${el.category}">
            <img src="${el.image}" alt="" class="work__tabs_img img"> </div>`
        });
    }
    
    if(incomingData.length === 0){
        workBtn.style.display = "none";
        return;
    }
};

//______________________________________________section work btn

const clientChoice = document.querySelector(".client__choice");

const cardList = document.querySelectorAll(".client__card");
const buttonsList = document.querySelectorAll(".client__card_l");
const totalCards = 3;

//{target, currentTarget}
const sliderMove = (event) => {
    const target = event.target;
    const currentTarget = event.currentTarget;
    let activeButton = currentTarget.querySelector(".client__card_l.active");
    let activeNumber = parseInt(activeButton.dataset.clientNumber);
    let activeCard = document.querySelector(".client__card.active");
    let prevCard = document.querySelector(".client__card.prev");
    let nextNumber = parseInt(target.dataset.clientNumber);

    

    if (target.classList.contains("arrow__left")) {
        nextNumber = activeNumber === 0 ? totalCards : activeNumber - 1;
    } else if (target.classList.contains("arrow__right")) {
        nextNumber = activeNumber === totalCards ? 0 : activeNumber + 1;
    }
    if(!nextNumber && nextNumber !== 0){
        return;
    }

    let nextCard = cardList[nextNumber];
    let nextButton = buttonsList[nextNumber];
    prevCard.classList.remove("prev");
    activeCard.classList.remove("active");
    activeCard.classList.add("prev");
    activeButton.classList.remove("active");
    nextCard.classList.add("active");
    nextButton.classList.add("active");
}

clientChoice.addEventListener("click", sliderMove)





