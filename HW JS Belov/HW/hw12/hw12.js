// TASK 1
//1. setTimeout() - виконує один раз, setInterval() - циклічно з інтервало до відміни

//2. Відпрацю миттєво тільки після завершення поточного коду

//2. Тому, що функція і всі її змінні будуть існувати допоки ми не викличемо clearInterval для її зупинкию Відповідно будуть займати місце в пам'яті
//



//TASK 2
let images = [...document.querySelectorAll(".image-to-show")];
images.forEach((el)=>{
    el.style.display = "none";
});
let stopButton = document.querySelector(".stop");
let startButton = document.querySelector(".start");

let idx = 0;
let saveIdx = 0;
let curImage = images[idx];
curImage.style.display = "block";
function getIdx(customIdx) {
    if (customIdx) {
        idx = customIdx;
    } else {
        if(idx < 3){
            idx ++;
        }else{
            idx = 0;
        }
        
    }

    return idx;
};
function renderImg() {
    let idx = getIdx();
    curImage.style.display = "none";
    curImage = images[idx];
    curImage.style.display = "block";
};
let clearSetInt = setInterval(renderImg, 2000);
stopButton.addEventListener('click', function(){
    saveIdx = idx;
    clearInterval(clearSetInt);
});
startButton.addEventListener('click', function(){
    getIdx(saveIdx);
    clearSetInt = setInterval(renderImg, 2000);
});
