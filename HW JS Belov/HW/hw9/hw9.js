// TASK 1


const tabs = document.querySelector(".tabs");

const tabsClickHandler = (e) => {

    const active = document.querySelector(".active");
    const item = e.target;
    


    active.classList.remove("active");
    item.classList.add("active");
    // item.classList.toggle("active");

    const elId = document.getElementById(`${item.textContent}`);
    
    if(item.textContent){
        let list = document.querySelectorAll(".tabs-content li");
        for(let i = 0; i < list.length; i++){
            if(list[i].style.display === "inline-block"){
                list[i].style.display = "none";
            }
        }
        elId.style.display = "inline-block";
        // elId.hidden = "false"
    }
};
 
tabs.addEventListener("click", tabsClickHandler)