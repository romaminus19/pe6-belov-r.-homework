//TASK 1

let w = [2,7,2,13,2];
let b = [5,1,10,2,451];
let finish = new Date(2022, 01, 14);

function workingCapacity(workSpeed, backLog, deadLine){
    function summ(arr){
        let res = 0;
        arr.forEach(element => {
            res += element;            
        });
        return res;
    }
    let start = new Date;

    let generalWorkerStoryPoint = summ(workSpeed);
    let generalBackLog = summ(backLog);

    let workerMakeTime = (generalBackLog / generalWorkerStoryPoint) * 8;

    let generalTime = ((deadLine - start) / 3600000) / 3;

    let inTime = (generalTime - workerMakeTime) / workerMakeTime;
    let wastedTime = (workerMakeTime - generalTime) / generalTime;

    if(workerMakeTime < generalTime){
        alert(`Все задачи будут успешно выполнены за ${Math.ceil(inTime)} дней до наступления дедлайна!`)
    }else{
        alert(`Команде разработчиков придется потратить дополнительно ${wastedTime} часов после дедлайна, чтобы выполнить все задачи в беклоге`)
    }

}
workingCapacity(w, b, finish);