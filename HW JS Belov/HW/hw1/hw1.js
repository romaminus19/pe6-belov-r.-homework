// before 22.11
// home work 1
//task 1

//Різниця між хзмінними var, let, const полягає у зоні видимості 
// та способами і можливістю змінювати дані в змінній

//Оголошувати змінну через Var є поганим тоном через те, що вона 
// є частиною старого синтаксису ESб а також через зону видимості(видно по всьому коді)

let name ;
let age ;
let shure;

do{
    name = prompt("What is your name?");
    age = prompt("How old are you?");
}while(name === ""  || isNaN(+age) || !name || typeof +age !== "number");

if(age < 18){
    alert("You are not allowed to visit this website")
}else if(age > 22){
    alert(`Welcome, ${name}`);
}else{
    shure = confirm("Are you sure you want to continue?");
    shure ? alert(`Welcome, ${name}`) : alert("You are not allowed to visit this website");
};



// before 

// let userVar = prompt("Please? Ent your num");

// for(let i = 0; i <= userVar; i++){
//     if(i%5 === 0 && i !== 0){
//         console.log(i);
//     }else if(i%5 !== 0){
//         continue;        
//     }else{
//         console.log('Sorry, no numbers');
//     }
// }
