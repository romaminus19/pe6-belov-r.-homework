// TASK 1

const change = document.querySelector(".button");
const main = document.querySelector(".main");
const currentTheme = localStorage.getItem("theme");
if(currentTheme === 'black'){
    main.classList.add("black");
}
change.addEventListener('click', fn);
function fn(){
    main.classList.toggle("black");
    let theme = "";
    if(main.classList.contains("black")){
        theme = "black";
    };
    localStorage.setItem("theme", theme);
}
