"use strict";
// TASK 1

// Екранування це спеціальні символи, за допомогою яких ми можемо вставляти в строки лапки, слеш, 
// і решту спец символів, що не прочитаються без допомоги спеціального синтаксису

// TASK 2
function createNewUser() {
    
    let firstName= prompt('Please, ent your name:');
    let lastName = prompt('Please, ent your surname:');
    let birthday = prompt('How old are you? Please, ent in format: dd.mm.yyyy');

    const newUser = {
        firstName,
        lastName,
        
        getLogin(){
            return firstName.charAt(0).toLowerCase() + lastName.toLowerCase();
        },
        getAge(){
            let writeDate = birthday.slice(6) + "-" + birthday.slice(3, 5) + "-" + birthday.slice(0, 2);
            let date = new Date(writeDate);
            let countAge = Date.now() - date;
            let userAge = countAge / (1000 * 60 * 60 * 24 * 365);
            return  Math.floor(userAge);
        },
        getPassword(){
            return firstName.charAt(0).toUpperCase() + lastName.toLowerCase() + birthday.slice(6);
        }
    }

    return newUser;
}

// console.log(createNewUser());
// console.log(createNewUser().getLogin());
// console.log(createNewUser().getAge());
console.log(createNewUser().getPassword());