"use strict";

// Task 1

// Цикл forEach проганяє кожне значення масиву через колбек функцію

// Task 2
// ____________________________________Var 1

// function filterBy(arr, type){
//     return arr.filter(item => typeof item !== type);
// }

 //____________________________________ Var 2
function filterBy(arr, type){
    let newArr = [];

    for(let i = 0; i < arr.length; i++){
        if(typeof arr[i] !==  type){
            newArr.push(arr[i]);
        }
    }

    return newArr;
}

let arrey = [1,2,3,'4','5',null, false, undefined, {}];



console.log(filterBy(arrey, 'string'));
console.log(filterBy(arrey, 'number'));
console.log(filterBy(arrey, 'object'));
console.log(filterBy(arrey, 'boolean'));
console.log(filterBy(arrey, 'function'));