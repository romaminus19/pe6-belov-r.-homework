"use strict";

// Before 25.11
// Task 1

// Функція необхідна для виклику коду (набору инструкцій) в необхіному для цього місця по ходу клду

// Аргументи, що передаються функції необхідні для отримання з функцїї очікуваного результату, якщо вона передбачає
// роботу з ними

// Task 2

// Level 1

// let num1 = prompt("Ent first num");
// let num2 = prompt("Ent second num");
// let operation = prompt("Enter desired operation (+, -, * or /)");
// let res;

// function calc(){
    
    
//     if(operation !== ""){
//         switch(operation){
//             case "+":
//              res = +num1 + +num2;
//              break;
//             case "-":
//              res = num1 - num2;
//              break;
//             case "/":
//              res = num1 / num2;
//              break;
//             case "*":
//              res = num1 * num2;
//              break;
//         }
//     }
//     return res;
// }

// console.log(calc());

// Level 2

let num1 = prompt("Ent first num");
let num2 = prompt("Ent second num");
let operation = prompt("Enter desired operation (+, -, * or /)");
let res;
function isNumber(x){
    return (!x || isNaN(+x) || typeof +x !== "number");
}

function calc(){
    if(isNumber(+num1) || isNumber(+num2)){
        num1 = prompt("Ent first num", num1);
        num2 = prompt("Ent second num", num2);
        operation = prompt("Enter desired operation (+, -, * or /)", operation);
    }
    
    if(operation !== ""){
        switch(operation){
            case "+":
             res = +num1 + +num2;
             break;
            case "-":
             res = +num1 - +num2;
             break;
            case "/":
             res = +num1 / +num2;
             break;
            case "*":
             res = +num1 * +num2;
             break;
        }
    }
    return res;
}

console.log(calc());