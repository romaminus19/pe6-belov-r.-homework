// TASK 1

//Це функція, яка відпрацбовує через певну дію, яку виконує користувач



//TASK 2

//main input
const main = document.createElement("div");
document.body.prepend(main);

const input = document.createElement("input");
input.id = "input";
const label = document.createElement("label");
label.setAttribute("for", "input");
label.textContent = "Price, $ ";

main.prepend(label);
main.append(input);
main.style.cssText = "padding: 10px";

//buble price
const span = document.createElement("span");

const button = document.createElement("button");
button.textContent = "X";
button.style.cssText = "border-radius: 15px; color: red; background-color: #fff"
const bublePrice = document.createElement("div");
bublePrice.style.cssText = "border: 1px solid #000; border-radius: 15px 15px 15px 0; padding: 3px; display: none; background-color: #91FAE5; margin: 0 10px;";

bublePrice.append(span);
bublePrice.append(button);
main.before(bublePrice);

//incorrect price value
const incorrectPrice = document.createElement("div");
incorrectPrice.style.cssText = "border: 1px solid #000; border-radius: 0 15px 15px 15px; padding: 3px; display: none; background-color: #E12525; margin: 0 10px;"
const faulseSpan = document.createElement("span");
faulseSpan.textContent = "Please enter correct price";
main.after(incorrectPrice);
incorrectPrice.append(faulseSpan);

// check number
function isNumber(x){
    return (!x || isNaN(+x) || typeof +x !== "number");
}


input.addEventListener("focus", () => input.style.cssText = "border-color: #00FF00; box-shadow: 2px 2px  #00FF00, -2px -2px  #00FF00, -2px 2px  #00FF00, 2px -2px  #00FF00;");


function btnFocusHandler(){
    
    if(!isNumber(input.value) && input.value >= 0){ //??????????
        bublePrice.style.cssText += "display: inline-block";
        span.textContent = `Текущая цена: ${input.value}`;
        input.style.cssText += "color: #00FF00";
        incorrectPrice.style.cssText += "display: none";
        
    }else{
        incorrectPrice.style.cssText += "display: inline-block";
        input.style.cssText = "border-color: #E12525; box-shadow: 2px 2px  #E12525, -2px -2px  #E12525, -2px 2px  #E12525, 2px -2px  #E12525; color: #E12525";
        bublePrice.style.cssText += "display: none";
    }

    
    button.addEventListener("click", ()=>{
        // bublePrice.remove();
        bublePrice.style.cssText += "display: none";
        input.value = "";
        input.style.cssText = "";
    });
    
}

input.addEventListener("focusout", btnFocusHandler);


